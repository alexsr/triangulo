'''
Program to produce a triangle with numbers
'''

import sys
number = 0

def line(number: int):
    linea = str(number) * number

    return linea


    """Return a string corresponding to the line for number"""


def triangle(number: int):
    if number > 9:
        raise ValueError("El parámetro debe ser del 1 al 9")
    triangulo = ""
    for i in range(1, number+1):
        triangulo = str (triangulo) + line(i) + "\n"

    return triangulo
    """Return a string corresponding to the triangle for number"""


def main():
    number: int = sys.argv[1]
    text = triangle(int(number))
    print(text)

if __name__ == '__main__':
    main()